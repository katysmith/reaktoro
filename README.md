# Reaktoro

Reaktoro is a unified framework for modeling chemically reactive systems. It provides methods for chemical equilibrium and kinetic calculations for multiphase systems. Reaktoro is mainly developed in C++ for performance reasons. A Python interface is available for a more convenient and simpler use. Currently, Reaktoro can interface with two widely used geochemical software: [PHREEQC](http://wwwbrr.cr.usgs.gov/projects/GWC_coupled/phreeqc/) and [GEMS](http://gems.web.psi.ch/). 

## Installation and Tutorials

For installation instructions, tutorials, and list of publications related to this project, please access [reaktoro.org](http://www.reaktoro.org). This web site describes how to download and install Reaktoro, and demonstrate some basic usage.


## License

Reaktoro is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Reaktoro is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Reaktoro. If not, see <http://www.gnu.org/licenses/>.

## Contact

For comments and requests, send an email to:

    allan.leal@erdw.ethz.ch